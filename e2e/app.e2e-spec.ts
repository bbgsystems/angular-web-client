import { AngularWebClientPage } from './app.po';

describe('angular-web-client App', () => {
  let page: AngularWebClientPage;

  beforeEach(() => {
    page = new AngularWebClientPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
