import {Component, OnInit} from "@angular/core";
import {User} from "../user";
import {UserService} from "../user.service";


@Component({
    selector: 'users',
    templateUrl: 'users.component.html',
})


export class UsersComponent {


    public users: User[];

    constructor(private userService: UserService){}

    ngOnInit() : void {
       this.userService.getUsers().subscribe(d => {
           this.users = d;
           console.log("Got users: " + JSON.stringify(this.users));
       });
    }







}