export class LoginResponse {
    id: String;
    token: String;
}