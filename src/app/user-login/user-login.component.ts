import {Component, Input} from "@angular/core";
import {LoginRequest} from "./login-request";
import {UserService} from "../user.service";
import {Router} from "@angular/router";

@Component({
    selector: 'user-login',
    templateUrl: 'user-login.component.html',
})


export class UserLoginComponent {

    @Input() loginRequest: LoginRequest

    constructor(private service: UserService,  private _router: Router){
        this.loginRequest = new LoginRequest();
        this.loginRequest.username = "tiger";
        this.loginRequest.password = "1234";
    }

    public login(){
        console.log('service call : useradd:', this.loginRequest); //
        this.service.loginUser(this.loginRequest).subscribe(s=> {
            if(s.status == 200){
                window.alert("User successfully logged in!")
                this._router.navigate(['protected']);

            }
        })
    }
}