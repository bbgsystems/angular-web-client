import {Component, OnInit, Input} from "@angular/core";
import {UserService} from "../user.service";
import {UserAdd} from "./user-add";

@Component({
    selector: 'user-add',
    templateUrl: 'user-add.component.html',
})


export class UserAddComponent {

    @Input() useradd: UserAdd;

    constructor(private service: UserService){
        this.useradd = new UserAdd();
        this.useradd.username = "tiger";
        this.useradd.phone = "0731446607";
        this.useradd.password = "1234";

    }

    public addUser(){
        console.log('service call : useradd:', this.useradd); //
        this.service.addUser(this.useradd).subscribe(s=>{
            if(s.status == 201){
                window.alert("User successfully added!")
            }
        })
    }

}