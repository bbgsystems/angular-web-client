import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule }    from '@angular/http';
import { AppComponent } from './app.component';
import {UserAddComponent} from "./user-add/user-add.component";
import {UserLoginComponent} from "./user-login/user-login.component";
import {UsersComponent} from "./user-list/users.component";
import {RecentLoginComponent} from "./recent-login/recent-login.component";
import {UserService} from "./user.service";
import {FormsModule} from "@angular/forms";
import { RouterModule }   from '@angular/router';
import {ProtectedComponent} from "./protected/protected.component";
import {LandingComponent} from "../landing/landing.component";

@NgModule({
    declarations: [
        AppComponent,
        UserAddComponent,
        UserLoginComponent,
        UsersComponent,
        RecentLoginComponent,
        ProtectedComponent,
        LandingComponent
    ],
    imports: [
        BrowserModule, HttpModule, FormsModule,
        RouterModule.forRoot([
            {
                path: 'protected',
                component: ProtectedComponent
            },
            {
                path: '',
                redirectTo: '/landing',
                pathMatch: 'full'
            },
            {
                path: 'login',
                component: UserLoginComponent
            },
            {
                path: 'add',
                component: UserAddComponent
            },
            {
                path: 'landing',
                component: LandingComponent
            }

        ])],


    providers: [UserService],
    bootstrap: [AppComponent]
})
export class AppModule { }
