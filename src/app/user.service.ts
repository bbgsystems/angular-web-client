import {Injectable} from "@angular/core";
import { UserAdd} from "./user-add/user-add";
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {User} from "./user";
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";
import {LoginRequest} from "./user-login/login-request";
import {LoginResponse} from "./user-login/login-response";


@Injectable()
export class UserService {

    private serverUrl = 'http://localhost:7070/api';  // URL to web api
    private headers = new Headers();

    public loggedIn : LoginResponse;

    constructor(private http: Http) {
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
    }

    public getUsers(): Observable<User[]> {
        return this.http
            .get(this.serverUrl + '/users', {headers: this.headers})
            .map(res => {
                if(res.status < 200 || res.status >= 300) {
                    throw new Error('This request has failed ' + res.status);
                }
                // If everything went fine, return the response
                else {
                    return res.json() as User[];
                }
            });

    }


    public addUser(userAdd : UserAdd ): Observable<any>{
        var data = JSON.stringify(userAdd);
        console.log("Service: Entered addUser Data:" + data)
        return this.http
            .put(this.serverUrl + '/user/add' , data, {headers: this.headers}).map(res => {
               return res;
            })
    }



    public loginUser(request : LoginRequest ): Observable<any>{
        var data = JSON.stringify(request);
        console.log("Service: Entered loginUser Data:" + data)
        return this.http
            .post(this.serverUrl + '/user/login' , data, {headers: this.headers}).map(res=> {
                console.log(" login server response" + res)
                this.loggedIn = res.json() as LoginResponse;
                return res;
            })

    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); //
        return Promise.reject(error.message || error);
    }

}